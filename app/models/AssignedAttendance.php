<?php

class AssignedAttendance extends \Eloquent {
	protected $table = 'assigned_attendance';

	protected $fillable = [];

	protected $guarded = ['id'];


	public function assigned_course(){
		return $this->belongsTo('AssignedCourse','assigned_course_id','id');
	}

	public function attendance(){
		return $this->hasMany('Attendance','assigned_attendance_id','id');
	}
}
