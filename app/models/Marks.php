<?php

class Marks extends \Eloquent {

	protected $table = 'marks';

	protected $fillable = [];

	protected $guarded = ['id'];


	public function termTest(){
		return $this->belongsTo('TermTest','term_test_id','id');
	}
}
