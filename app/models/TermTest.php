<?php

class TermTest extends \Eloquent {
	protected $table = 'term_test';

	protected $fillable = [];

	protected $guarded = ['id'];

		public function assignedCourse(){
			return $this->belongsTo('AssignedCourse','assigned_course_id','id');
		}

		public function marks(){
			return $this->hasMany('Marks','term_test_id','id');
		}
}
