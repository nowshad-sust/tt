<?php

class Dept extends \Eloquent {

	protected $table = 'dept';
	protected $fillable = [];
	protected $guarded = ['id'];

		public function userInfo(){
        return $this->hasMany('Userinfo', 'dept_id', 'id');
    }

		public function courseList(){
	        return $this->hasMany('CourseList', 'dept_id', 'id');
	    }

		public function studentList(){
		        return $this->hasMany('StudentList', 'dept_id', 'id');
		    }
}
