<?php

class Userinfo extends \Eloquent {
	protected $table = 'userinfo';

	protected $fillable = [];

	protected $guarded = ['id'];

		public function user(){
			return $this->belongsTo('User','user_id','id');
		}

		public function dept(){
			return $this->belongsTo('Dept','dept_id','id');
		}
}
