<?php

class AssignedCourse extends \Eloquent {
	protected $table = 'assigned_course';

	protected $fillable = [];

	protected $guarded = ['id'];

	protected $with = ['course'];

		public function user(){
			return $this->belongsTo('User','user_id','id');
		}

		public function course(){
			return $this->belongsTo('CourseList','course_id','id');
		}

		public function termTest(){
			return $this->hasMany('TermTest','assigned_course_id','id');
		}

		public function result(){
			return $this->hasOne('Result','assigned_course_id','id');
		}

		public function assigned_course(){
			return $this->hasOne('AssignedCourse','assigned_course_id','id');
		}


}
