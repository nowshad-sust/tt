<?php

class Result extends \Eloquent {
	protected $table = 'results';

	protected $fillable = [];

		public function assignedCourse(){
			return $this->belongsTo('AssignedCourse','assigned_course_id','id');
		}
}
