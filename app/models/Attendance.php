<?php

class Attendance extends \Eloquent {
	protected $table = 'attendance';

	protected $fillable = [];

	protected $guarded = ['id'];


	public function assignedAttendance(){
		return $this->belongsTo('AssignedAttendance','assigned_attendance_id','id');
	}
}
