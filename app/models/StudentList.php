<?php

class StudentList extends \Eloquent {
	protected $table = 'student_list';

	protected $fillable = [];

	protected $guarded = ['id'];

		public function dept(){
			return $this->belongsTo('Dept','dept_id','id');
		}
}
