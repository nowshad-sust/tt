<?php

class CourseList extends \Eloquent {
	protected $table = 'course_list';

	protected $fillable = [];

	protected $guarded = ['id'];

		public function dept(){
			return $this->belongsTo('Dept','dept_id','id');
		}

		public function assignedCourse(){
			return $this->hasMany('AssignedCourse','course_id','id');
		}
}
