<?php

return [
	'names' =>[
		'siteName' => 'Term Test Manager'
	],
	'roles' =>[
		'admin' => 'admin',
		'teacher' => 'teacher',
		'student' => 'student'
	]
];
