<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/',function(){
	return Redirect::route('course.assigned');
});

Route::group(['before' => 'guest'], function(){
	Route::controller('password', 'RemindersController');
	Route::get('login', ['as'=>'login','uses' => 'AuthController@login']);
	Route::post('login', array('uses' => 'AuthController@doLogin'));

	//register section
	Route::get('register', ['as'=>'register','uses' => 'UserController@registerForm']);
	Route::post('register', ['as'=>'register.post','uses' => 'UserController@registerPost']);

});

Route::group(array('before' => 'auth'), function()
{

	Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
	Route::get('dashboard', array('as' => 'dashboard', 'uses' => 'AuthController@dashboard'));
	Route::get('change-password', array('as' => 'password.change', 'uses' => 'AuthController@changePassword'));
	Route::post('change-password', array('as' => 'password.doChange', 'uses' => 'AuthController@doChangePassword'));

	//student CRUD
	Route::get('student/all', ['as' => 'student.all', 'uses' => 'StudentController@showAll']);
	Route::get('student/add', array('as' => 'student.add', 'uses' => 'StudentController@addForm'));
	Route::post('student/add', array('as' => 'student.add.post', 'uses' => 'StudentController@postAddForm'));
	Route::get('student/update/{student_id}', array('as' => 'student.update', 'uses' => 'StudentController@editForm'));
	Route::put('student/update/{student_id}', array('as' => 'student.update.post', 'uses' => 'StudentController@postEditForm'));
	Route::get('student/delete/{student_id}', array('as' => 'student.delete', 'uses' => 'StudentController@delete'));

	//course section
	Route::get('course', ['as' => 'course', 'uses' => 'CourseController@']);
	//course create
	Route::get('course/add', ['as' => 'course.add', 'uses' => 'CourseController@addCourseForm']);
	Route::post('course/add', ['as' => 'course.add.post', 'uses' => 'CourseController@addCoursePost']);
	//course update
	Route::get('course/update/{course_id}', ['as' => 'course.update.form', 'uses' => 'CourseController@updateCourseForm']);
	Route::put('course/update/{course_id}', ['as' => 'course.update.put', 'uses' => 'CourseController@updateCoursePut']);
	//course delete
	Route::get('course/delete/{course_id}', ['as' => 'course.delete', 'uses' => 'CourseController@deleteCourse']);

	Route::get('course/take', ['as' => 'course.take', 'uses' => 'CourseController@takeCourse']);
	Route::get('course/take/{course_id}', ['as' => 'course.take.form', 'uses' => 'CourseController@takeCourseForm']);
	Route::post('course/take/{course_id}', ['as' => 'course.take.post', 'uses' => 'CourseController@takeCoursePost']);


	Route::get('course/assigned', ['as' => 'course.assigned', 'uses' => 'MarksheetController@assignedCourses']);
	Route::get('course/assigned/{assigned_course_id}', ['as' => 'course.assigned.details', 'uses' => 'MarksheetController@assignedCourseDetails']);
	Route::get('course/assigned/{assigned_course_id}/{tt_id}', ['as' => 'course.assigned.tt', 'uses' => 'MarksheetController@assignedCourseTT']);
	Route::get('assigned/delete/{assigned_course_id}', ['as' => 'assigned.delete', 'uses' => 'MarksheetController@deleteAssignedCourse']);
	Route::get('tt/{assigned_course_id}/add', ['as' => 'tt.add', 'uses' => 'TermTestController@addTTForm']);

	Route::post('tt/{assigned_course_id}/add', ['as' => 'tt.add.post', 'uses' => 'TermTestController@addTTPost']);
	Route::get('tt/delete/{tt_id}', ['as' => 'tt.delete', 'uses' => 'TermTestController@deleteTT']);

	//ajax call
	Route::post('marks/{assigned_course_id}/{tt_id}/{mark_id}', ['as' => 'tt.mark.update', 'uses' => 'TermTestController@updateTTMark']);

	Route::get('choose/{assigned_course_id}/{tt_id}', ['as' => 'choose', 'uses' => 'MarksheetController@chooseForm']);

	Route::post('add/{assigned_course_id}/{tt_id}', ['as' => 'tt.add.mark', 'uses' => 'MarksheetController@postTTNewMark']);
	Route::get('delete/{mark_id}', ['as' => 'delete.mark', 'uses' => 'MarksheetController@deleteMark']);

	//project result sheet

	Route::get('project/{assigned_course_id}/bestone', ['as' => 'project.bestone', 'uses' => 'ResultController@projectBestOne']);
	Route::get('project/{assigned_course_id}/besttwo', ['as' => 'project.besttwo', 'uses' => 'ResultController@projectBestTwo']);
	Route::get('project/{assigned_course_id}/average', ['as' => 'project.average', 'uses' => 'ResultController@projectAverage']);

	Route::get('grace/{assigned_course_id}/bestone/{grace_point}', ['as' => 'grace.bestone', 'uses' => 'ResultController@graceBestOne']);
	Route::get('grace/{assigned_course_id}/besttwo/{grace_point}', ['as' => 'grace.besttwo', 'uses' => 'ResultController@graceBestTwo']);
	Route::get('grace/{assigned_course_id}/average/{grace_point}', ['as' => 'grace.average', 'uses' => 'ResultController@graceAverage']);


	//attendance
	Route::get('attendance/{assigned_course_id}', ['as' => 'attendance', 'uses' => 'ResultController@attendance']);
	Route::post('attendance/{attendance_id}', ['as' => 'attendance.update', 'uses' => 'ResultController@updateAttendance']);

	Route::post('totalclass/{assigned_attendance_id}', ['as' => 'totalclass.update', 'uses' => 'ResultController@updateTotalClass']);


	Route::get('generate/pdf', ['as' => 'generate.pdf', 'uses' => 'ResultController@generatePdf']);
	Route::get('publish/pdf', ['as' => 'publish.pdf', 'uses' => 'ResultController@publishPdf']);


});
