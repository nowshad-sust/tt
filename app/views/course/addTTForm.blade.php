@extends('layouts.default')
@section('content')
    @include('includes.alert')
    {{ Form::open(array('route' => ['tt.add.post',$assignedCourse->id], 'method' => 'post', 'role' => 'form')) }}
    <h2 class="form-signin-heading">Add term Test</h2>
    <div class="panel-body">

        {{ Form::label('tt_no', 'Term test serial number', array('' => '')) }}
        {{ Form::number('tt_no', null, array('class' => 'form-control')) }}

        {{ Form::label('date', 'Date', array('' => '')) }}
        {{ Form::text('date', null, array('id' => 'datepicker','class' => 'form-control')) }}

        <!--
        {{ Form::label('total_marks', 'Term test total marks', array('' => '')) }}
        {{ Form::number('total_marks', 20, array('class' => 'form-control')) }}
      -->
      
        {{ Form::submit('Add', array('class' => 'btn btn-lg btn-login btn-block')) }}
    </div>


    {{ Form::close() }}

@stop

@section('style')
{{ HTML::style('css/jquery-ui.css') }}
@stop
@section('script')
{{ HTML::script('js/jquery-1.10.2.js') }}
{{ HTML::script('js/jquery-ui.js') }}

<script>
$(function() {
  $( "#datepicker" ).datepicker();
});
</script>
@stop
