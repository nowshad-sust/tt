@extends('layouts.default')
@section('content')
    @include('includes.alert')
    {{ Form::model($course, array('route' => ['course.update.put', $course->id], 'method' => 'put', 'role' => 'form')) }}
    <h2 class="form-signin-heading">Add Course</h2>
    <div class="panel-body">

        {{ Form::label('dept', 'Department', array('' => '')) }}
        {{ Form::select('dept_id',$dept, null, array('class' => 'form-control')) }}

        {{ Form::label('semester', 'Semester', array('' => '')) }}
        {{ Form::select('semester',$semesterList, null, array('class' => 'form-control')) }}

        {{ Form::label('course_code', 'Course Code', array('' => '')) }}
        {{ Form::text('course_code', null, array('class' => 'form-control', 'autofocus')) }}

        {{ Form::label('course_title', 'Course Title', array('' => '')) }}
        {{ Form::text('course_title', null, array('class' => 'form-control', 'autofocus')) }}

        {{ Form::label('credit', 'Credit', array('' => '')) }}
        {{ Form::text('credit', null, array('class' => 'form-control', 'autofocus')) }}

        {{ Form::submit('Update', array('class' => 'btn btn-lg btn-login btn-block')) }}
    </div>

    {{ Form::close() }}


@stop
