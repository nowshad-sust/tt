@extends('layouts.default')
    @section('content')
        @include('includes.alert')

    <div class="panel-body">

    <h3 class="text-center">Assigned Course Details</h3>

    <section>
      <div>
        @if($assignedCourse->result)
        <h4>Reslt: {{ $assignedCourse->result->result_type }}</h4>
        Published to : {{ $assignedCourse->result->pdf_link }}
        @else
        Reslt: Not Published
        @endif
      </div>
      <br>
      <h4>
        Result options
      </h4>
      <div>
        <a href="{{route('attendance',$assignedCourse->id)}}" class="btn btn-xs btn-default">Attendance</a>
        <a href="{{route('project.bestone',$assignedCourse->id)}}" class="btn btn-success">Best one</a>
        <a href="{{route('project.besttwo',$assignedCourse->id)}}" class="btn btn-info">Best two</a>
        <a href="{{route('project.average',$assignedCourse->id)}}" class="btn btn-warning">Average</a>
      </div>

    </section>
        <a style="float:right;" href="{{route('tt.add',$assignedCourse->id)}}"><button class="btn btn-success">Add term test</button></a>
        <table class="display table table-bordered table-stripe" id="example">
            <thead>
            <tr>
                <th>ID</th>
                <th>Term test no</th>
                <th>total marks</th>
                <th>date</th>

                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($termTest as $Info)

                <tr class="">
                    <td>{{$Info->id}}</td>
                    <td>{{$Info->tt_no}}</td>
                    <td>{{$Info->total_marks}}</td>
                    <td>{{$Info->date}}</td>

                    <td class="text-center">
                      <a class="btn btn-xs btn-info btn-edit"
                         href="{{route('course.assigned.tt',[$assignedCourse->id,$Info->id])}}">
                         Marksheet
                      </a>
                      <a  href="{{route('tt.delete',$Info->id)}}"
                          class="btn btn-xs btn-danger btn-edit"
                          href="">Delete
                      </a>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
@stop

@section('style')
    {{ HTML::style('assets/data-tables/DT_bootstrap.css') }}

@stop

@section('script')
    {{ HTML::script('assets/data-tables/jquery.dataTables.js') }}
    {{ HTML::script('assets/data-tables/DT_bootstrap.js') }}

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {

            $('#example').dataTable({
            });
        });
    </script>
@stop
