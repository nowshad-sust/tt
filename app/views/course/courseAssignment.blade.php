@extends('layouts.default')
@section('content')
    @include('includes.alert')
    {{ Form::open(array('route' => ['course.take.post',$course->id], 'method' => 'post', 'role' => 'form')) }}

    <h2 class="form-signin-heading">Take a Course</h2>
    <span class="text-center"><h3>{{$course->course_code}}::{{$course->course_title}}</h3>
      <br><h4>for dept. of {{$course->dept->dept_full_name}}</h4>
      <br><h4>of {{$course->credit}} credits in semester-{{$course->semester}}</h4>
    </span>
    <div class="panel-body">

        {{ Form::label('batch', 'Batch', array('' => '')) }}
        {{ Form::select('batch',$batchList, null, array('class' => 'form-control')) }}
        <br>
        {{ Form::submit('Finalize taking this course', array('class' => 'btn btn-lg btn-info')) }}
    </div>

    {{ Form::close() }}


@stop
