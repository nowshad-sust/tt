@extends('layouts.default')
    @section('content')
        @include('includes.alert')

    <div class="panel-body">
      <h3 class="text-center">Assigned Course List</h3>
        <a data-toggle="modal" href="#myModal1"><button class="btn btn-success">Add new</button></a>
        <br>
        <span class="alt-success alert-success alert-dismissable"></span>

        <table class="display table table-bordered table-stripe" id="example">
            <thead>
            <tr>
                <th>ID</th>
                <th>Reg. no</th>
                <th class="text-center">Marks</th>

                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($marks as $Info)

                <tr class="">
                    <td class="id">{{$Info->id}}</td>
                    <td>{{$Info->registration_number}}</td>
                    <td class="text-center">

                          {{ Form::number('marks', $Info->marks, array('class' => 'form-control')) }}

                      <a href="#" class="btn btn-xs btn-success btn-update">update</a>

                    </td>

                    <td class="text-center">
                      <a class="btn btn-xs btn-info btn-edit"
                         href="{{route('course.assigned.details',$Info->id)}}">
                         Details
                      </a>
                      <a class="btn btn-xs btn-danger btn-edit"
                         href="{{route('delete.mark',$Info->id)}}">Delete
                      </a>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal1" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Student add form</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('route' => ['tt.add.mark',$course->id,$term_test->id], 'method' => 'post', 'role' => 'form')) }}
                    <h2 class="form-signin-heading">Add Student's Mark</h2>
                    <div class="panel-body">

                        {{ Form::label('registartion_number', 'Registration number', array('' => '')) }}
                        {{ Form::text('registration_number', null, array('class' => 'form-control', 'autofocus')) }}

                        {{ Form::label('marks', 'Marks', array('' => '')) }}
                        {{ Form::text('marks', null, array('class' => 'form-control', 'autofocus')) }}

                        <br>
                        <button data-dismiss="modal" class="btn btn-warning" type="button">Cancel</button>
                        {{ Form::submit('Add', array('class' => 'btn btn-success btn-login')) }}
                    </div>

                    {{ Form::close() }}

                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
    <!-- modal -->
@stop

@section('style')
    {{ HTML::style('assets/data-tables/DT_bootstrap.css') }}

@stop

@section('script')
    {{ HTML::script('assets/data-tables/jquery.dataTables.js') }}
    {{ HTML::script('assets/data-tables/DT_bootstrap.js') }}
@if($marks != null)
    <script type="text/javascript" charset="utf-8">
        $(".alt-success").hide();
        $(document).ready(function() {


            $('#example').dataTable({
              "bPaginate": false
            });

            $(".btn-update").click(function() {
              event.preventDefault();
                var tableData = $(this).parent().parent().children().eq(0).html();

                var marks = $(this).parent().children('input').val();

                //alert(marks);
                var url = '{{asset('/')}}' + 'marks/'+'{{$course->id}}'+'/'+'{{$term_test->id}}'+'/'+ tableData;

                $.ajax({
                  url: url,
                  data: {
                     format: 'json',
                     marks: marks
                  },
                  error: function() {
                     $('#info').html('<p>An error has occurred</p>');
                  },
                  success: function(data) {
                    //alert(data.message);
                    if(data.status == 'success'){
                        $(".alt-success").html('<div class="alert alert-success alert-dismissable fade in"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
                        +data.message+
                        '</div>');
                    }else{
                      $(".alt-success").html('<div class="alert alert-error alert-dismissable fade in"> '+data.message+'</div>');
                    }

                    $(".alt-success").show();


                  },
                  type: 'POST'
               });

                //alert(url);
            });

        });
    </script>
@endif

@stop
