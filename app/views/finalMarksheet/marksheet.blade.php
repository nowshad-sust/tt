@extends('layouts.default')
    @section('content')
        @include('includes.alert')

    <div class="panel-body">
      <h3 class="text-center">{{$title}}</h3>
        <a href="{{route('generate.pdf')}}"  class="btn btn-default">generate PDF</a>
        <a href="{{route('publish.pdf')}}"  class="btn btn-success">Publish PDF</a>

        {{ Form::label('grace', 'Grace', array('' => '')) }}
        @if($published)
        {{ Form::text('grace', $published->grace, array('id'=>'input','autofocus')) }}
        @else
        {{ Form::text('grace', null, array('id'=>'input','autofocus')) }}
        @endif
        <a id="link" class="bn btn-xs btn-info" href="#">Project Grace</a>
         max grace allowed = {{ $max_grace}}
         @if( Route::is('grace.*'))
         <h6>Current Grace Projection: {{$current_grace}}</h6>
         @endif
         <table class="display table table-bordered table-stripe" id="example">
            <thead>
            <tr>
                <th class="text-center">Reg. no</th>

                <th class="text-center">Marks</th>
            </tr>
            </thead>
            <tbody>
            @foreach($result as $key => $value)

                <tr class="">
                    <td class="text-center">{{$key}}</td>
                    <td class="text-center">{{ $value }}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>

@stop

@section('style')
    {{ HTML::style('assets/data-tables/DT_bootstrap.css') }}

@stop

@section('script')
    {{ HTML::script('assets/data-tables/jquery.dataTables.js') }}
    {{ HTML::script('assets/data-tables/DT_bootstrap.js') }}

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#example').dataTable({
              "bPaginate": false
            });
            $('#input').on('input', function() {
              var inputValue = this.value;
              var assigned_course_id = {{$published->assigned_course_id}};
              var result_type = '{{$result_type}}';

              var href = "{{asset('/')}}"+"grace/"+assigned_course_id+"/"+result_type+"/"+inputValue;

              $('#link').prop('href', href);
            });
        });
    </script>

@stop
