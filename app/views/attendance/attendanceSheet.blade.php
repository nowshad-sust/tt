@extends('layouts.default')
    @section('content')
        @include('includes.alert')

    <div class="panel-body">
      <h3 class="text-center">Attendance Sheet</h3>
        <span class="alt-success alert-success alert-dismissable"></span>
        <center><div id="result"></div></center>
        <div class="row">
          <div class="col-8-lg">
            Total Class Number:
            {{ Form::number('attendance', $assigned_attendance->total_class, array()) }}
            <a href="#" class="btn btn-xs btn-success btn-total-marks">update</a>
          </div>
        </div>
        <a data-toggle="modal" href="#myModal1"><button class="btn btn-success">Add new</button></a>
        <br>
        <table class="display table table-bordered table-stripe" id="example">
            <thead>
            <tr>
                <th>ID</th>
                <th>Reg. no</th>
                <th class="text-center">Attendance({{$assigned_attendance->total_class}} days total)</th>

                <th class="text-center">Marks(10)</th>

                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($attendance as $Info)

                <tr class="">
                    <td class="id">{{$Info->id}}</td>
                    <td>{{$Info->registration_number}}</td>
                    <td class="text-center">

                          {{ Form::number('attendance', $Info->attendance, array('class' => 'form-control')) }}

                      <a href="#" class="btn btn-xs btn-success btn-update">update</a>

                    </td>
                    @if($assigned_attendance->total_class)
                    <td>{{ round(($Info->attendance/$assigned_attendance->total_class)*10) }}</td>
                    @else
                    <td>enter total class number to see the marks</td>
                    @endif
                    <td class="text-center">
                      <a class="btn btn-xs btn-info btn-edit"
                         href="{{route('course.assigned.details',$Info->id)}}">
                         Details
                      </a>
                      <a class="btn btn-xs btn-danger btn-edit"
                         href="{{route('delete.mark',$Info->id)}}">Delete
                      </a>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div aria-hidden="true" arattendancebelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal1" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Student add form</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('method' => 'post', 'role' => 'form')) }}
                    <h2 class="form-signin-heading">Add Student's Mark</h2>
                    <div class="panel-body">

                        {{ Form::label('registartion_number', 'Registration number', array('' => '')) }}
                        {{ Form::text('registration_number', null, array('class' => 'form-control', 'autofocus')) }}

                        {{ Form::label('atendance', 'Attendence', array('' => '')) }}
                        {{ Form::text('attendance', null, array('class' => 'form-control', 'autofocus')) }}

                        <br>
                        <button data-dismiss="modal" class="btn btn-warning" type="button">Cancel</button>
                        {{ Form::submit('Add', array('class' => 'btn btn-success btn-login')) }}
                    </div>

                    {{ Form::close() }}

                </div>
                <div class="modal-footer">
                </div>

            </div>
        </div>
    </div>
    <!-- modal -->
@stop

@section('style')
    {{ HTML::style('assets/data-tables/DT_bootstrap.css') }}

@stop

@section('script')
    {{ HTML::script('assets/data-tables/jquery.dataTables.js') }}
    {{ HTML::script('assets/data-tables/DT_bootstrap.js') }}
@if($attendance != null)
    <script type="text/javascript" charset="utf-8">
        $(".alt-success").hide();
        $(document).ready(function() {


            $('#example').dataTable({
              "bPaginate": false
            });

            $(".btn-total-marks").click(function() {
              event.preventDefault();

                var total_class = $(this).parent().children('input').val();


                var url = '{{asset('/')}}'+'totalclass/'+'{{$assigned_attendance->id}}';
                //alert(url);

                $.ajax({
                  url: url,
                  data: {
                     format: 'json',
                     total_class: total_class
                  },
                  error: function() {
                    $("#result").html('Error!');
                    $("#result").addClass("alert alert-danger alert-dismissable fade in");
                  },
                  success: function(data) {
                    //alert(data.message);
                    if(data.status == 'success'){
                        $("#result").html('Successfully updated total class number!');
                        $("#result").addClass("alert alert-success alert-dismissable fade in");
                    }else{
                      $(".alt-success").html('<div class="alert alert-error alert-dismissable fade in"> '+data.message+'</div>');
                    }


                  },
                  type: 'POST'
               });


                //alert(url);
            });

            $(".btn-update").click(function() {
              event.preventDefault();
                var tableData = $(this).parent().parent().children().eq(0).html();

                var attendance = $(this).parent().children('input').val();


                var url = '{{asset('/')}}'+'attendance/'+tableData;
                //alert(attendance);

                $.ajax({
                  url: url,
                  data: {
                     format: 'json',
                     attendance: attendance
                  },
                  error: function() {
                     $('#info').html('<p>An error has occurred</p>');
                  },
                  success: function(data) {
                    //alert(data.message);
                    if(data.status == 'success'){
                        $(".alt-success").html('<div class="alert alert-success alert-dismissable fade in"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
                        +data.message+
                        '</div>');
                    }else{
                      $(".alt-success").html('<div class="alert alert-danger alert-dismissable fade in"> '+data.message+'</div>');
                    }

                    $(".alt-success").show();


                  },
                  type: 'POST'
               });

                //alert(url);
            });

        });
    </script>
@endif

@stop
