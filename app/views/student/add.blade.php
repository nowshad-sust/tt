@extends('layouts.default')
@section('content')
    @include('includes.alert')
    {{ Form::open(array('route' => 'student.add.post', 'method' => 'post', 'role' => 'form')) }}
    <h2 class="form-signin-heading">Add Student</h2>
    <div class="panel-body">

        {{ Form::label('dept', 'Department', array('' => '')) }}
        {{ Form::select('dept_id',$dept, null, array('class' => 'form-control')) }}

        {{ Form::label('batch', 'Batch', array('' => '')) }}
        {{ Form::text('batch', null, array('class' => 'form-control')) }}

        {{ Form::label('resgistartion_number', 'Registartion Number', array('' => '')) }}
        {{ Form::text('registration_number', null, array('class' => 'form-control', 'autofocus')) }}

        {{ Form::label('name', 'Name', array('' => '')) }}
        {{ Form::text('name', null, array('class' => 'form-control', 'autofocus')) }}

        {{ Form::submit('Add', array('class' => 'btn btn-lg btn-login btn-block')) }}

    </div>

    {{ Form::close() }}


@stop
