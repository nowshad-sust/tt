<aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">

                  <li class="sub-menu dcjq-parent-li">
                      <a href="javascript:;" class="dcjq-parent">
                          <i class="fa fa-laptop"></i>
                          <span>Course</span>
                      <span class="dcjq-icon"></span></a>
                      <ul class="sub" style="display: none;">
                          <li><a href="{{route('course.take')}}">Take a course</a></li>
                          <li><a href="{{route('course.assigned')}}">Assigned Courses</a></li>
                      </ul>
                  </li>

                  <li>

                      <a href="{{ URL::route('student.all') }}">
                          <i class="fa fa-dashboard"></i>
                          <span>Student List</span>
                      </a>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
