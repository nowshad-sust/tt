<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAssignedAttendanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('assigned_attendance', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('assigned_course_id')->unsigned();
			$table->integer('total_class')->nullable();
			$table->float('grace')->nullable();
			$table->timestamps();

			$table->foreign('assigned_course_id')->references('id')->on('assigned_course')->onUpdate('cascade')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('assigned_attendance');
	}

}
