<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_list', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('registration_number');
			$table->integer('dept_id')->unsigned();
			$table->integer('batch');
			$table->string('name');
			$table->timestamps();

			$table->foreign('dept_id')->references('id')->on('dept')->onUpdate('cascade')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('student_list');
	}

}
