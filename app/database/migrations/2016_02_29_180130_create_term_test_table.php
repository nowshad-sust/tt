<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTermTestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('term_test', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('assigned_course_id')->unsigned();
			$table->integer('tt_no');
			$table->integer('total_marks');
			$table->date('date');
			$table->timestamps();

			$table->foreign('assigned_course_id')->references('id')->on('assigned_course')->onUpdate('cascade')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('term_test');
	}

}
