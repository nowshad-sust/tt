<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttendanceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attendance', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('assigned_attendance_id')->unsigned();
			$table->string('registration_number');
			$table->integer('attendance')->nullable();
			$table->timestamps();

			$table->foreign('assigned_attendance_id')->references('id')->on('assigned_attendance')->onUpdate('cascade')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attendance');
	}

}
