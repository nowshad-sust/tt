<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('results', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('assigned_course_id')->unsigned();
			$table->string('result_type')->nullable();
			$table->float('grace')->nullable();
			$table->string('pdf_link')->nullable();
			$table->timestamps();

			$table->foreign('assigned_course_id')->references('id')->on('assigned_course')->onUpdate('cascade')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('results');
	}

}
