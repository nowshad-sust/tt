<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('course_list', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('course_code');
			$table->integer('dept_id')->unsigned();
			$table->string('course_title');
			$table->float('credit');
			$table->integer('semester');
			$table->timestamps();

			$table->foreign('dept_id')->references('id')->on('dept')->onUpdate('cascade')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('course_list');
	}

}
