<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class MarksTableSeeder extends Seeder {

	public function run()
	{
		$marks = [
					[
								'registration_number'  => 2012331002,
								'term_test_id'   => 1,
								'marks'		=>	15.5,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					],

					[
								'registration_number'  => 2012331005,
								'term_test_id'   => 1,
								'marks'		=>	15.5,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('marks')->insert($marks);
	}

}
