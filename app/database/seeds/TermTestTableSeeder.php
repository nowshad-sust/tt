<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class TermTestTableSeeder extends Seeder {

	public function run()
	{
		$term_test = [
					[
								'assigned_course_id'   => 1,
								'tt_no'	=> 1,
								'total_marks'	=> 20,
								'date' => date('Y-m-d',strtotime('2016-02-26')),
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('term_test')->insert($term_test);
	}

}
