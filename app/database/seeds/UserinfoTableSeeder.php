<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserinfoTableSeeder extends Seeder {

	public function run()
	{
		$userinfo = [
					[
								'user_id'	=>	1,
								'dept_id'   => 1,
								'name'  => 'Saif',
								'post'	=> 'Assistant Professor',
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('userinfo')->insert($userinfo);
	}

}
