<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AttendanceTableSeeder extends Seeder {

	public function run()
	{
		$attendance = [
					[
								'assigned_attendance_id'   => 1,
								'registration_number'  => '2012331002',
								'attendance'	=> 15,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					],
					[
								'assigned_attendance_id'   => 1,
								'registration_number'  => '2012331005',
								'attendance'	=> 13,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('attendance')->insert($attendance);
	}

}
