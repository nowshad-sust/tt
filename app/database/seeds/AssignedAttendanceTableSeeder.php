<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AssignedAttendanceTableSeeder extends Seeder {

	public function run()
	{
		$assigned_attendance = [
					[
								'assigned_course_id'   => 1,
								'total_class'  => 20,
								'grace'	=> 1.00,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('assigned_attendance')->insert($assigned_attendance);
	}

}
