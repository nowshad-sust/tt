<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CourseListTableSeeder extends Seeder {

	public function run()
	{
		$course_list = [
					[
								'course_code'	=>	'cse-133',
								'dept_id'   => 1,
								'course_title'  => 'C programming',
								'credit'	=> 3.00,
								'semester'	=> 1,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('course_list')->insert($course_list);
	}

}
