<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AssignedCourseTableSeeder extends Seeder {

	public function run()
	{
		$assigned_course = [
					[
								'course_id'	=>	1,
								'user_id'   => 2,
								'batch'  => 2012,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('assigned_course')->insert($assigned_course);
	}

}
