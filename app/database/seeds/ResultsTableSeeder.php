<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ResultsTableSeeder extends Seeder {

	public function run()
	{
		$result = [
					[
								'assigned_course_id'   => 1,
								'result_type'	=> 'best one',
								'grace'		=>	1.0,
								'pdf_link' => '/pdf/tt.pdf',
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('results')->insert($result);
	}

}
