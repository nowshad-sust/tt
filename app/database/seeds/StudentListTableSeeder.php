<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class StudentListTableSeeder extends Seeder {

	public function run()
	{
		$student_list = [
					[
								'registration_number'	=>	'2012331002',
								'dept_id'   => 1,
								'batch'  => 2012,
								'name'	=> 'Udoy',
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					],
					[
								'registration_number'	=>	'2012331005',
								'dept_id'   => 1,
								'batch'  => 2012,
								'name'	=> 'Farzad',
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					],
					[
								'registration_number'	=>	'2011331001',
								'dept_id'   => 1,
								'batch'  => 2011,
								'name'	=> 'Shafin',
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					],
					[
								'registration_number'	=>	'2012330002',
								'dept_id'   => 2,
								'batch'  => 2012,
								'name'	=> 'Tanvir',
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					]

		];

		DB::table('student_list')->insert($student_list);
	}

}
