<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		 $this->call('RolesTableSeeder');
		 $this->call('PermissionsTableSeeder');
		 $this->call('UsersTableSeeder');
		 $this->call('EntrustTableSeeder');
		 $this->call('DeptTableSeeder');
		 $this->call('UserinfoTableSeeder');
		 $this->call('StudentListTableSeeder');
		 $this->call('CourseListTableSeeder');
		 $this->call('AssignedCourseTableSeeder');
		 $this->call('TermTestTableSeeder');
		 $this->call('MarksTableSeeder');
	}

}
