<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class DeptTableSeeder extends Seeder {

	public function run()
	{
		$dept = [
					[
								'dept_short_name'  => 'CSE',
								'dept_full_name'   => 'Computer Science & Engineering',
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					],

					[
								'dept_short_name'  => 'EEE',
								'dept_full_name'   => 'Electrical & Electronic Engineering',
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
					],

		];

		DB::table('dept')->insert($dept);
	}

}
