<?php

class CourseController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /course
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	public function takeCourse()
	{
		$courses = CourseList::all();

		return View::make('course.select')->with('title','Take a course')
																			->with('courses',$courses);
	}

	public function addCourseForm(){

				$dept = Dept::lists('dept_short_Name','id');

        $semesterList = array(

            1   =>  '1/1',
            2   =>  '1/2',
            3   =>  '2/1',
            4   =>  '2/2',
            5   =>  '3/1',
            6   =>  '3/2',
            7   =>  '4/1',
            8   =>  '4/2',
            9   =>  '5/1',
            10  =>  '5/2'

        );

		return View::make('course.courseForm')->with('title','Add course')
																					->with('dept', $dept)
																					->with('semesterList', $semesterList);
	}

	public function addCoursePost(){
		try{

            $rules =[
                'dept_id'  		=>  'required',
                'semester'   	=>  'required',
                'course_code' =>  'required',
                'course_title'=>  'required',
                'credit'     	=>  'required|numeric'
            ];

            $data = Input::all();

            $validation = Validator::make($data,$rules);

            if($validation->fails()){
                return Redirect::back()->withErrors($validation)->withInput();
            }else{

                $Course = new CourseList();
                $Course->dept_id = $data['dept_id'];
                $Course->semester = $data['semester'];
                $Course->course_code = $data['course_code'];
                $Course->course_title = $data['course_title'];
                $Course->credit = $data['credit'];

                if($Course->save()){
                    return Redirect::route('course.take')->with('success','Course added');
                }else{
                    return Redirect::back()->withInput()->with('error','Course adding failed');
                }
            }

        }catch(Exception $ex){
            return Redirect::back()->withInput()->with(['error'=>'Course adding failed!']);
        }
	}

	public function takeCourseForm($course_id){

		try{

			$course = CourseList::find($course_id);

			if($course){

				$batchList = array(
					2011 =>	2011,
					2012 =>	2012,
					2013 =>	2013,
					2014 =>	2014,
					2015 =>	2015,
					2016 =>	2016,
				);

				return View::make('course.courseAssignment')->with('title','Course Assignment')
																										->with('course',$course)
																										->with('batchList',$batchList);
			}

		}catch(Exception $ex){
				return Redirect::back()->with('error','failed to assign course!');
		}

	}

	public function takeCoursePost($course_id){

		try{
            $rules =[
                'batch'  		=>  'required',
                ];

            $data = Input::all();

            $validation = Validator::make($data,$rules);

            if($validation->fails()){
                return Redirect::back()->withErrors($validation)->withInput();
            }else{

								$course = CourseList::find($course_id);

                $assignedCourse = new AssignedCourse();
								$assignedCourse->course_id = $course->id;
								$assignedCourse->user_id = Auth::user()->id;
                $assignedCourse->batch = $data['batch'];

                if($assignedCourse->save()){

									//add a result under this course
									$result = new Result();
									$result->assigned_course_id = $assignedCourse->id;
									$result->save();

									//add an attendace sheet
									$assigned_attendance = new AssignedAttendance();
									$assigned_attendance->assigned_course_id = $assignedCourse->id;
									$assigned_attendance->save();

									//create empty attendance sheet
									$assignedCourseWithCourse = AssignedCourse::with('course')->findOrFail($assignedCourse->id);
									$assignedStudents = StudentList::where('dept_id',$assignedCourseWithCourse->course->dept_id)
																									->where('batch',$assignedCourse->batch)
																									->get();

									if(count($assignedStudents) == 0){
											return Redirect::back()
																		->withInput()
																		->with('error','you have not any student list of
																						the corresponding batch & dept');
									}

									$attendanceAssignment = null;
									foreach($assignedStudents as $value){
										$attendanceAssignment[] = [
													'registration_number'  => $value->registration_number,
													'assigned_attendance_id'   => $assigned_attendance->id,
													'attendance'		=>	null,
													'created_at' => date('Y-m-d H:i:s'),
													'updated_at' => date('Y-m-d H:i:s')
										];
								}

							//return $marksAssignment;
							$assignment = DB::table('attendance')->insert($attendanceAssignment);

                    return Redirect::route('course.assigned')->with('success','Course added to your assigned course list');
                }else{
                    return Redirect::back()->withInput()->with('error','Course adding failed');
                }
            }

        }catch(Exception $ex){
            return Redirect::back()->withInput()->with(['error'=>'Course adding failed!']);
        }


	}

/**
	 * Show the form for creating a new resource.
	 * GET /course/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /course
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /course/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /course/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateCourseForm($course_id)
	{
		try{

			$course = CourseList::find($course_id);

			if($course){

				$dept = Dept::lists('dept_short_Name','id');

				$semesterList = array(

						1   =>  '1/1',
						2   =>  '1/2',
						3   =>  '2/1',
						4   =>  '2/2',
						5   =>  '3/1',
						6   =>  '3/2',
						7   =>  '4/1',
						8   =>  '4/2',
						9   =>  '5/1',
						10  =>  '5/2'

				);
					return View::make('course.updateCourseForm')->with('title', 'Course Edit')
																											->with('course', $course)
																											->with('dept', $dept)
																											->with('semesterList', $semesterList);

			}

		}catch(Exception $ex){
			return $ex;

			//return Redirect::back()->with('error','Course update form could not be generated!');
		}
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /course/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateCoursePut($course_id)
	{
		try{

            $rules =[
                'dept_id'  		=>  'required',
                'semester'   	=>  'required',
                'course_code' =>  'required',
                'course_title'=>  'required',
                'credit'     	=>  'required|numeric'
            ];

            $data = Input::all();

            $validation = Validator::make($data,$rules);

            if($validation->fails()){
                return Redirect::back()->withErrors($validation)->withInput();
            }else{

                $Course = CourseList::find($course_id);
                $Course->dept_id = $data['dept_id'];
                $Course->semester = $data['semester'];
                $Course->course_code = $data['course_code'];
                $Course->course_title = $data['course_title'];
                $Course->credit = $data['credit'];

                if($Course->save()){
                    return Redirect::route('course.take')->with('success','Course updated');
                }else{
                    return Redirect::back()->withInput()->with('error','Course update failed');
                }
            }

        }catch(Exception $ex){
            return Redirect::back()->withInput()->with(['error'=>'Course update failed!']);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /course/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteCourse($course_id)
	{
		try{

			$course = CourseList::find($course_id);

			if($course){
				if($course->delete()){

					return Redirect::route('course.take')->with('success','Course deleted successfully');
				}else{
					return Redirect::back()->with('error','Course deletion failed!');
				}
			}

		}catch(Exception $ex){
			return Redirect::back()->with('error','Course deletion failed!');
		}
	}

}
