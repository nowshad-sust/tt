<?php

class TermTestController extends \BaseController {

	public function addTTForm($assigned_course_id){
		try{

			$assignedCourse = AssignedCourse::find($assigned_course_id);

			if($assignedCourse){

				return View::make('course.addTTForm')->with('title','Add Term Test')
																							->with('assignedCourse', $assignedCourse);
			}else{
				return Redirect::back()->with('error','course not found!');
			}
		}catch(Exception $ex){
				return Redirect::back()->with('error','Failed!');
		}
	}

	public function deleteTT($tt_id){
		try{
			$tt = TermTest::find($tt_id);
			if($tt != null){
				if($tt->delete()){
					return Redirect::back()->with('success','term test deleted');
				}
			}
			return Redirect::back()->with('error','term could not be deleted');
		}catch(Exception $ex){
			return Redirect::back()->with('error','failed!');
		}

	}

	public function addTTPost($assigned_course_id){
		try{

			$rules =[
					//'total_marks'  		=>  'required|numeric',
					'tt_no'  					=>  'required|numeric',
					'date'						=>	'required|date'
			];

			$data = Input::all();

			$validation = Validator::make($data,$rules);

			if($validation->fails()){
					return Redirect::back()->withErrors($validation)->withInput();
			}else{

				$assignedCourse = AssignedCourse::with('course')->find($assigned_course_id);

				if($assignedCourse){

					$assignedStudents = StudentList::where('dept_id',$assignedCourse->course->dept_id)
																					->where('batch',$assignedCourse->batch)
																					->get();

					if(count($assignedStudents) == 0){
							return Redirect::back()
														->withInput()
														->with('error','you have not any student list of
																		the corresponding batch & dept');
					}

					$tt = new TermTest();
					$tt->assigned_course_id = $assignedCourse->id;
					$tt->tt_no = $data['tt_no'];
					$tt->total_marks = 20;//$data['total_marks'];
					$tt->date = date('Y-m-d',strtotime($data['date']));

					if($tt->save()){


						$marksAssignment = null;
						foreach($assignedStudents as $value){
							$marksAssignment[] = [
										'registration_number'  => $value->registration_number,
										'term_test_id'   => $tt->id,
										'marks'		=>	null,
										'created_at' => date('Y-m-d H:i:s'),
										'updated_at' => date('Y-m-d H:i:s')
							];
					}

				//return $marksAssignment;
				$assignment = DB::table('marks')->insert($marksAssignment);

				if($assignment){

					try{

						$course = $assignedCourse;
						$termTest = TermTest::where('id',$tt->id)->with('marks')->get();
						$marks = Marks::where('term_test_id',$tt->id)->get();

						return Redirect::back()->with('success', 'term test added');

						/*return View::make('course.marksheet')->with('title','Marksheet')
																								->with('term_test',$termTest)
																								->with('course',$course)
																								->with('marks',$marks);
						*/
					}catch(Exception $ex){
						return Redirect::back()
													->withInput()
					 								->with('error','tt marksheet generate failed');
					}
				}else{
					return Redirect::back()
												->withInput()
												->with('error','tt marksheet generate student assignment failed');
				}
					}
				}else{
					return Redirect::back()->with('error','course not found!');
				}

			}


		}catch(Exception $ex){
			return $ex;
				return Redirect::back()->with('error','Failed!');
		}
	}

	public function updateTTMark($assigned_course_id,$tt_id,$mark_id){

		$rules =[
				'marks'  		=>  'required|numeric'
		];

		$data = Input::all();

		$validation = Validator::make($data,$rules);

		if($validation->fails()){
			$data = ['status'=>'failed',
							 'message'=>'data update failed'];
			return Response::json($data);
		}else{

			$assigned_course = AssignedCourse::find($assigned_course_id);
			$tt = TermTest::find($tt_id);
			$mark = Marks::find($mark_id);

			$mark->marks = $data['marks'];

			if($mark->save()){
				$data = ['status'=>'success',
								 'message'=>'data updated successfully'];
				return Response::json($data);
			}else{
				$data = ['status'=>'failed',
								 'message'=>'data update failed'];
				return Response::json($data);
			}

		}

	}


}
