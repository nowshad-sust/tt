<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /user
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /user/create
	 *
	 * @return Response
	 */
	public function registerForm()
	{
		$posts = array(
			'student'	=>	'Student',
			'teacher'	=>	array(
				'lecturer'	=>	'Lecturer',
				'assistant professor'	=>	'assistant Professor',
				'professor'	=>	'professor'
			)

		);

		$depts = Dept::lists('dept_full_name','id');

		return View::make('auth.register')->with('title','Register')
																			->with('depts',$depts)
																			->with('posts',$posts);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /user
	 *
	 * @return Response
	 */
	public function registerPost()
	{
		$rules = array
		(
					'name'    => 'required',
					'dept'    => 'required',
					'post'    => 'required',
					'email'    => 'required|email|unique:users',
					'password' => 'required'
		);

		$allInput = Input::all();
		$validation = Validator::make($allInput, $rules);

		//dd($allInput);


		if ($validation->fails())
		{

			return Redirect::back()
						->withInput()
						->withErrors($validation);
		} else
		{
			$credentials = array
			(
						'name'    => Input::get('name'),
						'dept_id'    => Input::get('dept'),
						'post'    => Input::get('post'),
						'email'    => Input::get('email'),
						'password' => Input::get('password')
			);

			$user = new User();
			$user->email = $credentials['email'];
			$user->password = Hash::make($credentials['password']);
			$user->activation_status = true;

			if($user->save()){

				$role = Role::find(2);
      	$user->attachRole($role);

				$userinfo = new Userinfo();
				$userinfo->user_id = $user->id;
				$userinfo->dept_id = $credentials['dept_id'];
				$userinfo->name = $credentials['name'];
				$userinfo->post = $credentials['post'];

				if($userinfo->save()){
					return Redirect::route('login')->with('success', 'Registration successfull');
				}else{

					$user->delete();
					return Redirect::back()
								->withInput()
								->withErrors();
				}

			}else
			{
				$user->delete();
				return Redirect::back()
							->withInput()
							->withErrors();
			}
		}
	}

	/**
	 * Display the specified resource.
	 * GET /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /user/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /user/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
