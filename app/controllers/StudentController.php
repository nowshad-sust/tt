<?php

class StudentController extends \BaseController {

	public function showAll()
	{
		$students = StudentList::with('dept')->get();

		return View::make('student.menu')->with('title','Student Menu')
																			->with('students',$students);
	}

	public function addForm()
	{
		$dept = Dept::lists('dept_short_name','id');

		return View::make('student.add')->with('title','Add Student')
																		->with('dept',$dept);
	}

	public function postAddForm(){

            $rules =[
                'dept_id'  		=>  'required',
                'batch'   	=>  'required|numeric',
                'name' =>  'required',
                'registration_number'=>  'required|numeric|unique:student_list,registration_number'
            ];

            $data = Input::all();

            $validation = Validator::make($data,$rules);

            if($validation->fails()){
                return Redirect::back()->withErrors($validation)->withInput();
            }else{

                $student = new StudentList();
                $student->dept_id = $data['dept_id'];
                $student->name = $data['name'];
                $student->batch = $data['batch'];
                $student->registration_number = $data['registration_number'];

                if($student->save()){

                    return Redirect::back()->with('success','student added');
                }else{
                    return Redirect::back()->withInput()->with('error','add student failed');
                }
            }
		}

		public function editForm($student_id)
		{
			$student = StudentList::find($student_id);

			$dept = Dept::lists('dept_short_name','id');

			return View::make('student.update')->with('title','Update Student')
																				->with('dept',$dept)
																				->with('student',$student);
		}

		public function postEditForm($student_id){

	            $rules =[
	                'dept_id'  		=>  'required',
	                'batch'   	=>  'required|numeric',
	                'name' =>  'required',
	                'registration_number'=>  'required|numeric'
	            ];

	            $data = Input::all();

	            $validation = Validator::make($data,$rules);

	            if($validation->fails()){
	                return Redirect::back()->withErrors($validation)->withInput();
	            }else{

	                $student = StudentList::find($student_id);
	                $student->dept_id = $data['dept_id'];
	                $student->name = $data['name'];
	                $student->batch = $data['batch'];
	                $student->registration_number = $data['registration_number'];

	                if($student->save()){

	                    return Redirect::back()->with('success','student added');
	                }else{
	                    return Redirect::back()->withInput()->with('error','add student failed');
	                }
	            }
			}


	public function delete($student_id)
	{
		try{

			$student = StudentList::find($student_id);

			if($student->delete()){
				return Redirect::back()->with('success','student deleted');
			}else{
					return Redirect::back()->with('error','student delition failed');
			}

		}catch(Exception $ex){

		}
	}

}
