<?php

class MarksheetController extends \BaseController {

	public function assignedCourses(){

		$user = Auth::user();

		$assignedCourses = AssignedCourse::where('user_id',$user->id)
																			->with('course')
																			->get();

		return View::make('course.assignedCourses')->with('title','Assigned Course')
																							 ->with('assignedCourses', $assignedCourses);

	}

	public function assignedCourseDetails($assigned_course_id){

		$user = Auth::user();

		$assignedCourse = AssignedCourse::where('user_id',$user->id)
																			->where('id',$assigned_course_id)
																			->with('course')
																			->with('result')
																			->first();

		$termTest = TermTest::where('assigned_course_id',$assigned_course_id)
													->with('assignedCourse')
													->get();


		return View::make('course.assignedCourseDeatils')->with('title','Course Details')
																							 			->with('assignedCourse', $assignedCourse)
																										->with('termTest',$termTest);

	}

	public function assignedCourseTT($assigned_course_id,$tt_id){

		try{

			$course = AssignedCourse::find($assigned_course_id);
			$tt = TermTest::where('id',$tt_id)->with('marks')->first();
			$marks = Marks::where('term_test_id',$tt_id)->get();
			//return $tt->id;

			return View::make('course.marksheet')->with('title','Marksheet')
																					->with('term_test',$tt)
																					->with('course',$course)
																					->with('marks',$marks);

		}catch(Exception $ex){

		}

	}


	/**
	 * Show the form for creating a new resource.
	 * GET /marksheet/create
	 *
	 * @return Response
	 */
	public function deleteAssignedCourse($assigned_course_id)
	{
		try{

			$assignedCourse = AssignedCourse::find($assigned_course_id);

			if($assignedCourse->delete()){
				return Redirect::back()->with('success','deleted assigned course');
			}

		}catch(Exception $ex){

		}

		//
	}

	public function chooseForm($assigned_course_id, $tt_id)
	{

	}

	/**
	 * Display the specified resource.
	 * GET /marksheet/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postTTNewMark($assigned_course_id, $tt_id)
	{

				            $rules =[
				                'marks'   	=>  'required',
				                'registration_number'=>  'required|numeric'
				            ];

				            $data = Input::all();

										//return $data;

				            $validation = Validator::make($data,$rules);

				            if($validation->fails()){
				                return Redirect::back()->withErrors($validation)->withInput();
				            }else{
												$assignedCourse = AssignedCourse::find($assigned_course_id);
												$tt = TermTest::find($tt_id);

												if($assignedCourse && $tt){

													$mark = new Marks();
													$mark->term_test_id = $tt->id;
													$mark->marks = $data['marks'];
													$mark->registration_number = $data['registration_number'];

													if($mark->save()){

															return Redirect::back()->with('success','student mark added');
													}else{
															return Redirect::back()->withInput()->with('error','add student mark failed');
													}
												}

				            }



	}

	public function deleteMark($mark_id){
		$mark = Marks::find($mark_id);

		if($mark){
			if($mark->delete()){
				return Redirect::back()->with('success', 'mark deleted');
			}else{
				return Redirect::back()->with('error', 'mark deletion failed!');
			}
		}
	}


}
