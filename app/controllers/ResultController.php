<?php

class ResultController extends \BaseController {

	public function attendance($assigned_course_id){

		try{

			$assigned_course = AssignedCourse::findOrFail($assigned_course_id);

			if($assigned_course){
				$assigned_attendance = AssignedAttendance::where('assigned_course_id',$assigned_course->id)
																									->first();

				$attendance = Attendance::where('assigned_attendance_id',$assigned_attendance->id)
																->orderBy('registration_number','ASC')
																->get();


				//return $attendance;

				return View::make('attendance.attendanceSheet')
										->with('title','Attendance')
										->with('assigned_attendance',$assigned_attendance)
										->with('attendance',$attendance)
										->with('assigned_course',$assigned_course);

			}

		}catch(Exception $ex){
			return Redirect::back()->with('error','Failed!');
		}

	}

	public function updateAttendance($attendance_id){
		try{

			$rules =[
					'attendance'  		=>  'required|numeric'
			];

			$data = Input::all();

			$validation = Validator::make($data,$rules);

			if($validation->fails()){
				$data = ['status'=>'failed',
								 'message'=>'data update failed'];
				return Response::json($data);
			}else{

				$attendance = Attendance::find($attendance_id);

				$attendance->attendance = $data['attendance'];

				if($attendance->save()){
					$data = ['status'=>'success',
									 'message'=>'data updated successfully'];
					return Response::json($data);
				}else{
					$data = ['status'=>'failed',
									 'message'=>'data update failed'];
					return Response::json($data);
				}

			}
		}catch(Exception $ex){
			$data = ['status'=>'failed',
							 'message'=>'data update failed'];
			return Response::json($data);
		}
	}

	public function updateTotalClass($assigned_attendance_id){
		try{

			$rules =[
					'total_class'  		=>  'required|numeric'
			];

			$data = Input::all();

			$validation = Validator::make($data,$rules);

			if($validation->fails()){
				$data = ['status'=>'failed',
								 'message'=>'data update failed'];
				return Response::json($data);
			}else{

				$assigned_attendance = AssignedAttendance::findOrFail($assigned_attendance_id);

				$assigned_attendance->total_class = $data['total_class'];

				if($assigned_attendance->save()){
					$data = ['status'=>'success',
									 'message'=>'data updated successfully'];
					return Response::json($data);
				}else{
					$data = ['status'=>'failed',
									 'message'=>'data update failed'];
					return Response::json($data);
				}

			}
		}catch(Exception $ex){
			$data = ['status'=>'failed',
							 'message'=>'data update failed'];
			return Response::json($data);
		}
	}


	public function projectBestOne($assigned_course_id){

			try{

				$studentMarks = $this->getMarksArray($assigned_course_id);

				//take maxx from each student record

				$result = [];
				$max_number = 0;
				foreach ($studentMarks as $key => $student) {
					rsort($student);
					$result[$key] = $student[0];
					if($max_number < $student[0]){
						$max_number = $student[0];
					}
				}

				$max_grace = 20/$max_number;

				$published = Result::where('assigned_course_id',$assigned_course_id)->first();
				//storing the session variable
				$course = AssignedCourse::with('course')->find($assigned_course_id);
				$info['semester'] = $course->course->semester;
				$info['batch'] = $course->batch;
				$info['course_code'] = $course->course->course_code;
				$info['course_title'] = $course->course->course_title;
				$info['dept'] = $course->course->dept_id;
				$info['assigned_course_id'] = $assigned_course_id;
				$info['result_type'] = 'best one';
				$info['grace'] = $published->grace;

				Session::set('results', $result);
				Session::set('information', $info);

				return View::make('finalMarksheet.marksheet')->with('title','Best One')
																										->with('result',$result)
																										->with('published',$published)
																										->with('studentmarks', $studentMarks)
																										->with('result_type','bestone')
																										->with('max_grace',$max_grace);

																									}catch(Exception $ex){
																										return Redirect::back()->with('error','Failed!');
																									}
			}

			public function graceBestOne($assigned_course_id,$grace_point){

try{
						$studentMarks = $this->getMarksArray($assigned_course_id);

						//take maxx from each student record

						$result = [];
						$max_number = 0;
						foreach ($studentMarks as $key => $student) {
							rsort($student);
							$result[$key] = round($student[0]*$grace_point);
							if($max_number < $student[0]){
								$max_number = $student[0];
						}
					}
							$max_grace = 20/$max_number;

						//storing the session variable
						$course = AssignedCourse::with('course')->find($assigned_course_id);
						$info['semester'] = $course->course->semester;
						$info['batch'] = $course->batch;
						$info['course_code'] = $course->course->course_code;
						$info['course_title'] = $course->course->course_title;
						$info['dept'] = $course->course->dept_id;
						$info['assigned_course_id'] = $assigned_course_id;
						$info['result_type'] = 'best one';
						$info['grace'] = $grace_point;

						Session::set('results', $result);
						Session::set('information', $info);

						$published = Result::where('assigned_course_id',$assigned_course_id)->first();


						return View::make('finalMarksheet.marksheet')->with('title','Grace Best One')
																												->with('result',$result)
																												->with('result_type','bestone')
																												->with('studentmarks', $studentMarks)
																												->with('published',$published)
																												->with('max_grace',$max_grace)
																												->with('current_grace',$grace_point);
																											}catch(Exception $ex){
																												return Redirect::back()->with('error','Failed!');
																											}
					}



			public function projectBestTwo($assigned_course_id){

try{
						$studentMarks = $this->getMarksArray($assigned_course_id);

						$published = Result::where('assigned_course_id',$assigned_course_id)->first();
						//take maxx from each student record
						$result = [];
						$max_number = 0;
						foreach ($studentMarks as $key => $student) {
							rsort($student);
							$best1 = $student[0];
							$best2 = $student[1];
							$avg = round(($best1 + $best2) / 2);

							$result[$key] = $avg;

							if($max_number < $avg){
								$max_number = $avg;
							}
						}

						$max_grace = 20/$max_number;

						//storing the session variable
						$course = AssignedCourse::with('course')->find($assigned_course_id);
						$info['semester'] = $course->course->semester;
						$info['batch'] = $course->batch;
						$info['course_code'] = $course->course->course_code;
						$info['course_title'] = $course->course->course_title;
						$info['dept'] = $course->course->dept_id;
						$info['assigned_course_id'] = $assigned_course_id;
						$info['result_type'] = 'best two';
						$info['grace'] = $published->grace;

						Session::set('results', $result);
						Session::set('information', $info);

						return View::make('finalMarksheet.marksheet')->with('title','Best Two')
																												->with('result',$result)
																												->with('published',$published)
																												->with('max_grace',$max_grace)
																												->with('result_type', 'besttwo')
																												->with('studentmarks', $studentMarks);
																											}catch(Exception $ex){
																												return Redirect::back()->with('error','Failed!');
																											}
					}

					public function graceBestTwo($assigned_course_id, $grace_point){
try{
								$studentMarks = $this->getMarksArray($assigned_course_id);

								//take maxx from each student record

								$published = Result::where('assigned_course_id',$assigned_course_id)->first();

								$result = [];
								$max_number = 0;
								foreach ($studentMarks as $key => $student) {
									rsort($student);
									$best1 = $student[0];
									$best2 = $student[1];
									$avg = round(($best1 + $best2) / 2);

									$result[$key] = round($avg*$grace_point);

									if($max_number < $avg){
										$max_number = $avg;
									}

								}

								$max_grace = 20/$max_number;

								//storing the session variable
								$course = AssignedCourse::with('course')->find($assigned_course_id);
								$info['semester'] = $course->course->semester;
								$info['batch'] = $course->batch;
								$info['course_code'] = $course->course->course_code;
								$info['course_title'] = $course->course->course_title;
								$info['dept'] = $course->course->dept_id;
								$info['assigned_course_id'] = $assigned_course_id;
								$info['result_type'] = 'best two';
								$info['grace'] = $grace_point;

								Session::set('results', $result);
								Session::set('information', $info);

								return View::make('finalMarksheet.marksheet')->with('title','Best Two')
																														->with('result',$result)
																														->with('published',$published)
																														->with('current_grace',$published->grace)
																														->with('result_type','besttwo')
																														->with('max_grace',$max_grace)
																														->with('studentmarks', $studentMarks);
																													}catch(Exception $ex){
																														return Redirect::back()->with('error','Failed!');
																													}
							}

					public function projectAverage($assigned_course_id){

try{
								$studentMarks = $this->getMarksArray($assigned_course_id);

								//take maxx from each student record
								$published = Result::where('assigned_course_id',$assigned_course_id)->first();

								$result = [];
								$max_number = 0;
								foreach ($studentMarks as $key => $student) {
									$avg = 0;
									for($i = 0;$i < count($student);$i++)
									$avg = ($student[$i] + $avg) / 2;

									$result[$key] = round($avg);
									if($max_number < $avg){
										$max_number = $avg;
									}
								}

								$max_grace = 20/$max_number;

								//storing the session variable
								$course = AssignedCourse::with('course')->find($assigned_course_id);
								$info['semester'] = $course->course->semester;
								$info['batch'] = $course->batch;
								$info['course_code'] = $course->course->course_code;
								$info['course_title'] = $course->course->course_title;
								$info['dept'] = $course->course->dept_id;
								$info['assigned_course_id'] = $assigned_course_id;
								$info['result_type'] = 'average';
								$info['grace'] = $published->grace;


								Session::set('results', $result);
								Session::set('information', $info);

								return View::make('finalMarksheet.marksheet')->with('title','Average')
																														->with('result',$result)
																														->with('published',$published)
																														->with('result_type','average')
																														->with('max_grace',$max_grace)
																														->with('studentmarks', $studentMarks);
																													}catch(Exception $ex){
																														return Redirect::back()->with('error','Failed!');
																													}
							}

							public function graceAverage($assigned_course_id, $grace_point){
try{
										$studentMarks = $this->getMarksArray($assigned_course_id);

										//take maxx from each student record

										$published = Result::where('assigned_course_id',$assigned_course_id)->first();

										$result = [];
										$max_number = 0;
										foreach ($studentMarks as $key => $student) {
											$avg = 0;
											for($i = 0;$i < count($student);$i++)
											$avg = ($student[$i] + $avg) / 2;

											$result[$key] = round($avg * $grace_point);

											if($max_number < $avg){
												$max_number = $avg;
											}
										}

										$max_grace = 20/$max_number;

										//storing the session variable
										$course = AssignedCourse::with('course')->find($assigned_course_id);
										$info['semester'] = $course->course->semester;
										$info['batch'] = $course->batch;
										$info['course_code'] = $course->course->course_code;
										$info['course_title'] = $course->course->course_title;
										$info['dept'] = $course->course->dept_id;
										$info['assigned_course_id'] = $assigned_course_id;
										$info['result_type'] = 'average';
										$info['grace'] = $grace_point;


										Session::set('results', $result);
										Session::set('information', $info);

										return View::make('finalMarksheet.marksheet')->with('title','Average')
																																->with('result',$result)
																																->with('published',$published)
																																->with('current_grace',$grace_point)
																																->with('result_type','average')
																																->with('max_grace',$max_grace)
																																->with('studentmarks', $studentMarks);
																															}catch(Exception $ex){
																																return Redirect::back()->with('error','Failed!');
																															}
									}

							function generatePdf(){
try{
								$result = Session::get('results');
								$information = Session::get('information');

							  //return View::make('finalMarksheet.demo')->with('result',$result);
							  $table = "
								<!DOCTYPE html>
								<html>
								<head>
								<title>Term Test Marks</title>

								<style>
								  .center{
								    text-align: center;
								  }
								</style>
								</head>
								<body>
								  <div class='center'>
								  <img src='".public_path('img/SUST_Color.png')."'width='100px'>
								  <h3 style='text-align:center;'>ShahJalal University of Science & Technology, Sylhet</h3>
								  <h5>Semester:".$information['semester']." Session:  ".$information['batch']." </h5>
									<h5>Course Code: ".$information['course_code']."
									Course Title: ".$information['course_title']."
									<br>Total Marks: . . . . . . . . .</h5>
								  <table align='center' border='1' style='border:1px dotted black;width:80%;border-collapse:collapse;'>
								  <tr>
								  <th style='padding:3px;'>Registration Number</th><th style='padding:3px;'>Marks</th>
								  </tr>
							  ";
							      foreach($result as $key => $value){
							          $table .=  "<tr>
							                <td align='center'>".$key."</td>
							                <td align='center'>".$value."</td>
							              </tr>";
							          }

							  $table .= "
								</table>
								   <pre><h5>Course teacher: ".Auth::user()->userInfo->name."             Signature: . . . . . .  <h5></pre>
								   <pre><h5>Department: . . . . . . . . .           Date: . . . . . . .</h5></pre>
								</div>
								</body>
								</html>
								";

							    return $pdf = PDF::load($table, 'A4', 'portrait')->show();
								}catch(Exception $ex){
									return Redirect::back()->with('error','Failed!');
								}
							}

							function publishPdf(){
try{
								$result = Session::get('results');
								$information = Session::get('information');

							  //return View::make('finalMarksheet.demo')->with('result',$result);
								$table = "
								<!DOCTYPE html>
								<html>
								<head>
								<title>Term Test Marks</title>

								<style>
								  .center{
								    text-align: center;
								  }
								</style>
								</head>
								<body>
								  <div class='center'>
								  <img src='".public_path('img/SUST_Color.png')."'width='100px'>
								  <h3 style='text-align:center;'>ShahJalal University of Science & Technology, Sylhet</h3>
								  <h5>Semester:".$information['semester']." Session:  ".$information['batch']." </h5>
									<h5>Course Code: ".$information['course_code']."
									Course Title: ".$information['course_title']."
									<br>Total Marks: . . . . . . . . .</h5>
								  <table align='center' border='1' style='border:1px dotted black;width:80%;border-collapse:collapse;'>
								  <tr>
								  <th style='padding:3px;'>Registration Number</th><th style='padding:3px;'>Marks</th>
								  </tr>
							  ";
							      foreach($result as $key => $value){
							          $table .=  "<tr>
							                <td align='center'>".$key."</td>
							                <td align='center'>".$value."</td>
							              </tr>";
							          }

							  $table .= "
								</table>
								   <pre><h5>Course teacher: ".Auth::user()->userInfo->name."             Signature: . . . . . .  <h5></pre>
								   <pre><h5>Department: . . . . . . . . .           Date: . . . . . . .</h5></pre>
								</div>
								</body>
								</html>
								";

									$pdfPath = public_path("/")
									."pdf/"
									.$information['assigned_course_id']
									."-".$information['course_code']
									."-".$information['batch']
									.".pdf";
									$file = File::put($pdfPath, PDF::load($table, 'A4', 'portrait')->output());

									//save file publishing info database
									if($file){
										$result = Result::where('assigned_course_id',$information['assigned_course_id'])->first();

										if(!$result){
											$result = new Result();
										}
										$result->assigned_course_id =	$information['assigned_course_id'];
										$result->result_type = $information['result_type'];
										$result->pdf_link = $pdfPath;
										$result->grace = $information['grace'];

										if($result->save()){
											return Redirect::back()->with('success','result saved successfully to: '.$pdfPath);
										}else {
											return Redirect::back()->with('error','result could not be saved!');
										}

									}else {
										return Redirect::back()->with('error','Pdf File could not be saved!');
									}
								}catch(Exception $ex){
									return Redirect::back()->with('error','Failed!');
								}
							}

	public function getMarksArray($assigned_course_id){
try{
			$assignedCourse = AssignedCourse::find($assigned_course_id);

			if($assignedCourse){
				$tt = TermTest::where('assigned_course_id',$assignedCourse->id)
												->with('marks')
												->get();
				//make marks array of each student

				$tt_ids = [];
				$students = [];

				foreach ($tt as $single_tt) {
					$tt_ids[] = $single_tt->id;
				}

				foreach ($tt_ids as $id) {
					$term_test = TermTest::where('id',$id)->with('marks')->first();
					foreach ($term_test->marks as $marks) {
							$students[] = $marks->registration_number;
					}
				}

				$unique = array_unique($students);

				$array = [];
				foreach($unique as $k=>$v) {
					$array[] = $v;
				}

				sort($array);
				//sorted array of studentlist

				//get corresponding marks

				$studentMarks = [];

				foreach ($array as $student){
					foreach($tt_ids as $tt_id){

						$mark = Marks::where('term_test_id', $tt_id)
													->where('registration_number',$student)
													->pluck('marks');

						if($mark){
								$studentMarks[$student][] = $mark;
						}else{
							$studentMarks[$student][] = "0";
						}

					}
				}

				return $studentMarks;

			}
		}catch(Exception $ex){
			return Redirect::back()->with('error','Failed!');
		}
	}

}
